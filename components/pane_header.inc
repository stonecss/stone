<?php

/**
 * Preprocess variables for pane-header.tpl.php
 */
function stone_preprocess_pane_header(&$variables) {
  $link_options = array(
    'html' => TRUE,
    'attributes' => array('title' => '← Back to home page'),
  );

  $site_name = filter_xss_admin(variable_get('site_name', 'Drupal'));

  // Branding - logo.
  $variables['branding_logo'] = '';
  $logo_path = DRUPAL_ROOT . parse_url(theme_get_setting('logo'), PHP_URL_PATH);
  if (file_exists($logo_path)) {
    $logo = theme('image', array(
      'path' => $variables['logo'],
      'alt' => $site_name . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => 'Branding-logo'),
    ));
    $variables['branding_logo'] = l($logo, '<front>', $link_options);
  }

  // Branding - name.
  $variables['branding_name'] = '';
  if (theme_get_setting('toggle_name')) {
    $variables['branding_name'] = theme('html_tag', array(
      'element' => array(
        '#tag' => drupal_is_front_page() ? 'h1' : 'div',
        '#value' => l($site_name, '<front>', $link_options),
        '#attributes' => array(
          'class' => array('Branding-name'),
        ),
      ),
    ));
  }

  // Branding - slogan.
  $variables['branding_slogan'] = '';
  if (theme_get_setting('toggle_slogan')) {
    $variables['branding_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}
