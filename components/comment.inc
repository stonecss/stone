<?php

/**
 * Preprocess variables for comment.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case).
 */
function stone_preprocess_comment(&$variables, $hook) {
  $variables['object_type'] = 'comment';

  // Make the comment available to the template via a generic variable.
  $variables['object'] = $variables['comment'];

  // Keep consistent variables across similar templates (comment, node, etc).
  $variables['user_picture'] = $variables['picture'];

  // Always display comment titles.
  $variables['display_title'] = TRUE;
  $variables['title_tag'] = 'h3';

  // Always display submission date/time information on comments.
  $variables['display_submitted'] = TRUE;

  //
  // Attributes
  //
  $attributes['class'][] = 'Comment';

  if (!empty($variables['comment']->new)) {
    $attributes['class'][] = 'Comment--new';
  }

  // Status
  $status = 'published';
  if (isset($variables['comment']->in_preview)) {
    $status = 'preview';
  }
  elseif ($variables['comment']->status == COMMENT_NOT_PUBLISHED) {
    $status = 'unpublished';
  }
  $attributes['class'][] = 'Comment--' . $status;

  // Author
  if (empty($variables['comment']->uid)) {
    $attributes['class'][] = 'Comment--byAnonymous';
  }
  else {
    if ($variables['comment']->uid == $variables['node']->uid) {
      $attributes['class'][] = 'Comment--byNodeAuthor';
    }
    if ($variables['comment']->uid == $variables['user']->uid) {
      $attributes['class'][] = 'comment--byViewer';
    }
  }

  $attributes['id'] = 'comment-' . $variables['comment']->cid;
  $variables['attributes_array'] = $attributes;

  // Content attributes.
  $variables['content_attributes_array']['class'][] = 'Comment-content';
}
