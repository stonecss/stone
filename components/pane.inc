<?php

/**
 * Preprocess variables for panels-pane.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("panels_pane" in this case).
 */
function stone_preprocess_panels_pane(&$variables, $hook) {
  stone_preprocess_box($variables, $hook);

  $pane = $variables['pane'];

  if (!empty($pane->css)) {
    $variables['attributes_array']['class'] = array_merge(
      $variables['attributes_array']['class'],
      explode(' ', $pane->css['css_class'])
    );
  }

  // Display only the content for the following pane types.
  $content_only_panes = array('pane_header', 'page_content', 'pane_content_header');
  if (in_array($pane->type, $content_only_panes)) {
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }

  switch ($pane->subtype) {
    case 'menu_block-stone-core-main-menu':
      if ($pane->panel == 'navigation') {
        $variables['attributes_array']['class'][] = 'Box--mainMenu';
        $variables['attributes_array']['data-offcanvas-panel'] = 'primary';
      }
      break;
  }
}

/**
 * Process variables panels-pane.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("box" in this case).
 */
function stone_process_panels_pane(&$variables, $hook) {
  // Render content.
  // $variables['content'] = render($variables['content']);
}
