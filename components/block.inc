<?php

/**
 * Preprocess variables for block.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case).
 */
function stone_preprocess_block(&$variables, $hook) {
  stone_preprocess_box($variables, $hook);

  $block = $variables['block'];

  $variables['title'] = !empty($block->subject) ? $block->subject : '';

  // Add classes to attributes array.
  foreach ($variables['classes_array'] as $class) {
    // Ignore core block classes.
    if (strpos($class, 'block') !== 0) {
      $variables['attributes_array']['class'][] = $class;
    }
  }

  // Display only the content for the following blocks.
  $content_only_blocks = array('system_main');
  $block_name = $block->module . '_' . $block->delta;
  if (in_array($block_name, $content_only_blocks)) {
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }
}

/**
 * Process variables block.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("box" in this case).
 */
function stone_process_block(&$variables, $hook) {
  // Render content.
  // $variables['content'] = trim(render($variables['content']));
}

/**
 * Implements hook_block_view_alter().
 */
function stone_block_view_alter(&$data, $block) {
  // Add inline modifier class to menu blocks in navigation region.
  if ($block->region == 'navigation') {
    // Does this block contain a menu?
    $is_menu_block = ((
         $block->module == 'system'
      && in_array($block->delta, array_keys(menu_get_menus())))
      || in_array($block->module, array('menu', 'menu_block'))
    );

    if ($is_menu_block) {
      $content = &$data['content'];

      // The menu_block module puts content in #content.
      if ($block->module == 'menu_block' && isset($content['#content'])) {
        $content = $content['#content'];
      }

      $content['#attributes']['class'][] = 'List--inline';
    }
  }
}
