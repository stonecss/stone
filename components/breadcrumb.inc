<?php

/**
 * Overrides theme_breadcrumb().
 */
function stone_breadcrumb($variables) {
  if (!empty($variables['breadcrumb'])) {
    $breadcrumb = $variables['breadcrumb'];

    // Add separators if there is more than one item.
    if (($count = count($breadcrumb)) > 1) {
      for ($i = 0; $i < $count - 1; ++$i) {
        $breadcrumb[$i] .= '<i class="Breadcrumb-separator Icon--angleRight"></i>';
      }
    }

    $output = '<div class="Breadcrumb">';

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .u-hiddenVisually.
    $output .= '<h2 class="u-hiddenVisually">' . t('You are here') . '</h2>';

    $output .= theme('item_list', array(
      'items' => $breadcrumb,
      'type' => 'ol',
    )) . '</div>';

    return $output;
  }
}
