<?php

/**
 * Preprocess variables for theme_menu_link().
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case).
 */
function stone_preprocess_menu_link(&$variables, $hook) {
  $classes = array('Nav-item');

  // Prepend state classes with 's-' and remove others.
  foreach ($variables['element']['#attributes']['class'] as $class) {
    switch ($class) {
      case 'active-trail':
        $classes[] = 'is-active';
        break;
      case 'expanded':
        $classes[] = 'is-expanded';
        break;
      case 'collapsed':
        $classes[] = 'is-collapsed';
        break;
    }
  }
  $variables['element']['#attributes']['class'] = $classes;

  // Remove link classes.
  unset($variables['element']['#localized_options']['attributes']['class']);
}

/**
 * Preprocess variables for theme_menu_tree().
 */
function stone_preprocess_menu_tree(&$variables) {
  // Add default class for menu.
  $variables['attributes_array']['class'][] = 'Nav';

  // Add modifier class based on menu name.
  if (!empty($variables['original_tree'])) {
    $menu_item_keys = element_children($variables['original_tree']);
    $menu_item = $variables['original_tree'][reset($menu_item_keys)];

    if (isset($menu_item['#original_link']['menu_name']) && function_exists('stone_string_to_camelcase')) {
      $modifier = stone_string_to_camelcase($menu_item['#original_link']['menu_name']);
      $variables['attributes_array']['class'][] = 'Nav--' . $modifier;
    }
  }
}

/**
 * Overrides theme_menu_tree().
 */
function stone_menu_tree($variables) {
  if (!isset($variables['attributes_array'])) {
    $variables['attributes_array'] = array();
  }
  return '<ul' . drupal_attributes($variables['attributes_array']) . '>' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_local_tasks().
 *
 * Returns HTML for primary and secondary local tasks.
 *
 * @param $variables
 *   An associative array containing:
 *     - primary: (optional) An array of local tasks (tabs).
 *     - secondary: (optional) An array of local tasks (tabs).
 *
 * @ingroup themeable
 * @see menu_local_tasks()
 */
function stone_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<ul class="Nav Nav--tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<ul class="Nav Nav--pills">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= render($variables['secondary']);
  }

  return $output;
}

/**
 * Overrides theme_menu_local_task().
 *
 * Returns HTML for a single local task link.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element containing:
 *     - #link: A menu link array with 'title', 'href', and 'localized_options'
 *       keys.
 *     - #active: A boolean indicating whether the local task is active.
 *
 * @ingroup themeable
 */
function stone_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="u-hiddenVisually"> ' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  // Tab attributes.
  $attributes_array = array(
    'class' => array('Nav-item'),
  );

  if (!empty($variables['element']['#active'])) {
    $attributes_array['class'][] = 'is-active';
  }

  return '<li' . drupal_attributes($attributes_array) . '>' . l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
}

/**
 * Returns HTML for a single local action link.
 *
 * @param $variables
 *   An associative array containing:
 *   - element: A render element containing:
 *     - #link: A menu link array with 'title', 'href', and 'localized_options'
 *       keys.
 *
 * @ingroup themeable
 */
function stone_menu_local_action($variables) {
  $link = $variables['element']['#link'];

  // Add button classes.
  $options = isset($link['localized_options']) ? $link['localized_options'] : array();
  $options['attributes']['class'][] = 'button button--small';

  // Add plus icon.
  $options['html'] = TRUE;
  $link['title'] = '<span class="icon--plus"></span> ' . $link['title'];

  $output = '<li>';
  if (isset($link['href'])) {
    $output .= l($link['title'], $link['href'], $options);
  }
  elseif (!empty($link['localized_options']['html'])) {
    $output .= $link['title'];
  }
  else {
    $output .= check_plain($link['title']);
  }
  $output .= "</li>\n";

  return $output;
}
