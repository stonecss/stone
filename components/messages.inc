<?php

/**
 * Overrides theme_status_messages().
 */
function stone_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status messages'),
    'error' => t('Error messages'),
    'warning' => t('Warning messages'),
  );

  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"AlertBox is-$type\" data-alertbox>\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="AlertBox-title u-hiddenVisually">' . $status_heading[$type] . "</h2>\n";
    }

    if (count($messages) > 1) {
      $output .= " <ul class=\"AlertBox-content\">\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= '<div class="AlertBox-content">' . $messages[0] . '</div>';
    }
    $output .= "</div>\n";
  }

  if (!empty($output)) {
    $output = '<div id="messages">' . $output . '</div>';
  }

  return $output;
}
