<?php

/**
 * Implements hook_entity_view_alter().
 */
function stone_entity_view_alter(&$build) {
  // Tidy up classes on node/comment links.
  if (isset($build['links'])) {
    $classes = &$build['links']['#attributes']['class'];

    // Add inline list modifier class if this list should be inline.
    if (in_array('inline', $classes)) {
      array_unshift($classes, 'List--inline');
    }

    // Remove core Drupal classes.
    $classes = array_diff($classes, array('links', 'inline'));
  }
}
