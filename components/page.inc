<?php

/**
 * Preprocess variables for page.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case).
 */
function stone_preprocess_page(&$variables, $hook) {
  // Branding - logo.
  $logo_path = DRUPAL_ROOT . parse_url($variables['logo'], PHP_URL_PATH);
  if (file_exists($logo_path)) {
    $variables['branding_logo'] = theme('image', array(
      'path' => $variables['logo'],
      'alt' => $variables['site_name'] . "'s logo",
      'title' => NULL,
      'width' => NULL,
      'height' => NULL,
      'attributes' => array('class' => 'Branding-logo'),
    ));
  }
  else {
    $variables['branding_logo'] = '';
  }

  // Branding - name.
  $name_wrapper = drupal_is_front_page() ? 'h1' : 'div';
  $variables['branding_name'] = '<' .$name_wrapper .' class="Branding-name">'.
                                $variables['site_name'] .
                                '</' .$name_wrapper .'>';

  // Branding - slogan.
  $variables['branding_slogan'] = $variables['site_slogan'];
}
