<?php

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case).
 */
function stone_preprocess_html(&$variables, $hook) {
  // Attributes for html element.
  $variables['html_attributes_array'] = array(
    'class' => array('no-js'),
    'lang' => $variables['language']->language,
    'dir' => $variables['language']->dir,
  );

  // Send X-UA-Compatible HTTP header to force IE to use the most recent
  // rendering engine or use Chrome's frame rendering engine if available.
  // This also prevents the IE compatibility mode button to appear when using
  // conditional classes on the html tag.
  if (is_null(drupal_get_http_header('X-UA-Compatible'))) {
    drupal_add_http_header('X-UA-Compatible', 'IE=edge,chrome=1');
  }

  // Add a class that tells us whether we're on the front page or not.
  $variables['attributes_array']['class'][] = $variables['is_front'] ? 'Page--front' : 'Page--notFront';

  // Add panel classes.
  $panel_body_css = &drupal_static('panel_body_css');
  if (!empty($panel_body_css['body_classes_to_add'])) {
    $variables['attributes_array']['class'][] = check_plain($panel_body_css['body_classes_to_add']);
  }

  // Add a class that tells us whether the page is viewed by an authenticated user or not.
  $variables['attributes_array']['class'][] = $variables['logged_in'] ? 'is-loggedIn' : 'is-notLoggedIn';
}

/**
 * Process variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function stone_process_html(&$variables, $hook) {
  global $theme;

  // Add default css if stone is the active theme. This is not needed for
  // subthemes as they should only include the parts of stone they require with
  // their own custom configuration.
  if ($theme == 'stone') {
    $variables['styles'] = drupal_get_css();
  }

  // Flatten out html_attributes.
  $variables['html_attributes'] = drupal_attributes($variables['html_attributes_array']);
}

/**
 * Override or insert variables in the html_tag theme function.
 */
function stone_process_html_tag(&$variables) {
  $tag = &$variables['element'];

  if ($tag['#tag'] == 'style' || $tag['#tag'] == 'script') {
    // Remove redundant type attribute and CDATA comments.
    unset($tag['#attributes']['type'], $tag['#value_prefix'], $tag['#value_suffix']);

    // Remove media="all" but leave others unaffected.
    if (isset($tag['#attributes']['media']) && $tag['#attributes']['media'] === 'all') {
      unset($tag['#attributes']['media']);
    }
  }
}

/**
 * Implement hook_html_head_alter().
 */
function stone_html_head_alter(&$head) {
  // Simplify the meta tag for character encoding.
  if (isset($head['system_meta_content_type']['#attributes']['content'])) {
    $content = $head['system_meta_content_type']['#attributes']['content'];
    $head['system_meta_content_type']['#attributes'] = array(
      'charset' => str_replace('text/html; charset=', '', $content),
    );
  }
}

