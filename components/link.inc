<?php

/**
 * Preprocess function for theme('link').
 *
 * Remove the 'active' class from anchor tags since they are not required.
 */
function stone_preprocess_link(&$variables) {
  if (!empty($variables['options']['attributes']['class'])) {
    $class = &$variables['options']['attributes']['class'];

    if (is_array($class)) {
      $class = array_diff($class, array('active'));
    }
    elseif (strstr($class, 'active')) {
      $class = str_replace('active', '', $class);
    }
  }
}
