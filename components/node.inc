<?php

/**
 * Preprocess variables for node.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case).
 */
function stone_preprocess_node(&$variables, $hook) {
  $variables['object_type'] = 'node';

  // Make the node available to the template via a generic variable.
  $variables['object'] = $variables['node'];

  // Display node titles when not viewing the full page view.
  if ($variables['display_title'] = $variables['view_mode'] != 'full') {
    // Use an h3 for the node title when rendering anything other than the full
    // view since there should be a "context setting" h2 higher up the document.
    $variables['title_tag'] = 'h3';
    $variables['title'] = l($variables['title'], 'node/' . $variables['nid'], array('html' => TRUE));
  }

  //
  // Attributes
  //
  $attributes['class'][] = 'Node';
  $attributes['class'][] = 'Node--' . stone_string_to_camelcase($variables['type']);
  $attributes['class'][] = 'Node--' . $variables['view_mode'];
  $attributes['class'][] = 'is-' . ($variables['status'] ? '' : 'un') . 'published';

  if (!empty($variables['promote'])) {
    $attributes['class'][] = 'Node--promoted';
  }
  if (!empty($variables['sticky'])) {
    $attributes['class'][] = 'Node--sticky';
  }
  if (!empty($variables['comment_count']) > 0) {
    $attributes['class'][] = 'Node--hasComments';
  }

  $attributes['id'] = 'node-' . $variables['nid'];
  $variables['attributes_array'] = $attributes;

  // Content attributes.
  $variables['content_attributes_array']['class'][] = 'Node-content';

  // Make it possible to preprocess specific node bundles.
  $preprocess_bundle = 'preprocess_node__' . $variables['type'];
  $names = array_merge(array('template'), module_implements($preprocess_bundle));

  foreach ($names as $name) {
    $function = $name . '_' . $preprocess_bundle;

    if (function_exists($function)) {
      $function($variables, $hook);
    }
  }

  // Update class for title span in read more link to 'u-hiddenVisually'.
  if (isset($variables['content']['links']['node']['#links']['node-readmore'])) {
    $node_title_stripped = strip_tags($variables['node']->title);
    $variables['content']['links']['node']['#links']['node-readmore'] = array(
      'title' => t('Read more<span class="u-hiddenVisually"> about @title</span>', array('@title' => $node_title_stripped)),
      'href' => 'node/' . $variables['node']->nid,
      'html' => TRUE,
      'attributes' => array('rel' => 'tag', 'title' => $node_title_stripped),
    );
  }
}

/**
 * Process variables for node.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case).
 */
function stone_process_node(&$variables, $hook) {
  // Render content.
  foreach ($variables['content'] as $key => $value) {
    // $variables[$key] = render($value);
  }

  hide($variables['content']['comments']);
  hide($variables['content']['links']);
  // $variables['content'] = trim(render($variables['content']));
}
