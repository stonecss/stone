<?php

/**
 * Preprocess variables box.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("box" in this case).
 */
function stone_preprocess_box(&$variables, $hook) {
  $variables['attributes_array']['class'] = array('Box');
  $variables['title_attributes_array']['class'] = array('Box-title');
  $variables['content_attributes_array']['class'] = array('Box-content');
  $variables['box_prefix'] = '';
  $variables['box_suffix'] = '';
  $variables['admin_links'] = '';
  $variables['links'] = '';
  $variables['feeds'] = '';
  $variables['more'] = '';
}
