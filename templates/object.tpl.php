<?php

/**
 * @file
 * Default theme implementation to display an object (node, comment, etc).
 *
 * @ingroup themeable
 */
?>
<div<?php print $attributes; ?>>
  <?php print $user_picture; ?>

  <?php if ($display_title): ?>
    <?php print render($title_prefix); ?>
      <<?php print $title_tag . $title_attributes; ?>>
        <?php print $title; ?>
      </<?php print $title_tag; ?>>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <?php if ($display_submitted): ?>
    <div class="<?php print ucfirst($object_type); ?>-submitted">
      <?php if (!empty($permalink)): ?>
        <?php print $permalink; ?>
      <?php endif; ?>

      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($content)): ?>
    <div<?php print $content_attributes; ?>>
      <?php print render($content); ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($signature)): ?>
    <div class="User-signature">
      <?php print $signature ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)): ?>
    <?php print $links; ?>
  <?php endif; ?>

  <?php if (!empty($comments)): ?>
    <?php print $comments; ?>
  <?php endif; ?>
</div>
