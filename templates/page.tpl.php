<header class="Container Container--header" role="header">
  <div class="Container-inner">
    <div class="Grid">
      <div class="Grid-cell">
        <div class="Branding">
          <?php print $branding_logo; ?>
          <?php print $branding_name; ?>
          <?php if ($branding_slogan): ?>
            <div class="Branding-slogan"><?php print $branding_slogan; ?></div>
          <?php endif; ?>
        </div>
      </div>

      <?php print render($page['header']); ?>
    </div>
  </div>
</header>

<?php if ($page['navigation']): ?>
<nav class="Container Container--navigation" role="navigation">
  <div class="Container-inner">
    <div class="Grid">
      <?php print render($page['navigation']); ?>
    </div>
  </div>
</nav>
<?php endif; ?>

<?php if ($page['top']): ?>
<div class="Container Container--top">
  <div class="Container-inner">
    <div class="Grid">
      <?php print render($page['top']); ?>
    </div>
  </div>
</div>
<?php endif; ?>

<main class="Container Container--main">
  <div class="Container-inner">
    <div class="Grid">
      <div class="Grid-cell<?php if (!empty($primary_classes)) { print $primary_classes; } ?>">
        <div class="Container Container--primary">
          <header>
            <?php if ($breadcrumb): ?>
              <?php print $breadcrumb; ?>
            <?php endif; ?>

            <?php if ($messages): ?>
              <?php print $messages; ?>
            <?php endif; ?>

            <?php print render($title_prefix); ?>
            <?php if ($title): ?>
            <h1 id="PageTitle"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>

            <?php if ($tabs): ?>
            <div class="Tabs">
              <?php print render($tabs); ?>
            </div>
            <?php endif; ?>

            <?php if ($page['help']): ?>
            <div id="Help">
              <?php print render($page['help']); ?>
            </div>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="ActionLinks">
                <?php print render($action_links); ?>
              </ul>
            <?php endif; ?>
          </header>

          <?php if ($page['content_top']): ?>
          <div class="Grid">
            <?php print render($page['content_top']); ?>
          </div>
          <?php endif; ?>

          <?php if ($page['content']): ?>
          <div class="Grid">
            <?php print render($page['content']); ?>
          </div>
          <?php endif; ?>

          <?php print $feed_icons; ?>

          <?php if ($page['content_bottom']): ?>
          <div class="Grid">
            <?php print render($page['content_bottom']); ?>
          </div>
          <?php endif; ?>
        </div>
      </div>

      <?php if ($page['secondary']): ?>
        <div class="Grid-cell<?php print $secondary_classes; ?>">
          <div class="Container Container--secondary">
            <div class="Grid">
              <?php print render($page['secondary']); ?>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if ($page['tertiary']): ?>
        <div class="Grid-cell<?php print $tertiary_classes; ?>">
          <div class="Container Container--tertiary">
            <div class="Grid">
              <?php print render($page['tertiary']); ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    </div>
  </div>
</main>


<?php if ($page['bottom']): ?>
<div class="Container Container--bottom">
  <div class="Container-inner">
    <div class="Grid">
      <?php print render($page['bottom']); ?>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if ($page['footer']): ?>
<footer class="Container Container--footer">
  <div class="Container-inner">
    <div class="Grid">
      <?php print render($page['footer']); ?>
    </div>
  </div>
</footer>
<?php endif; ?>

