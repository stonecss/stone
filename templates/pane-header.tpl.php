<div class="Branding">
  <?php print $branding_logo; ?>
  <?php print $branding_name; ?>
  <?php if ($branding_slogan): ?>
    <div class="Branding-slogan"><?php print $branding_slogan; ?></div>
  <?php endif; ?>
</div>
