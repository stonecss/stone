<?php

require_once 'components/block.inc';
require_once 'components/box.inc';
require_once 'components/breadcrumb.inc';
require_once 'components/button.inc';
require_once 'components/comment.inc';
require_once 'components/form.inc';
require_once 'components/html.inc';
require_once 'components/item_list.inc';
require_once 'components/menu.inc';
require_once 'components/messages.inc';
require_once 'components/node.inc';
require_once 'components/page.inc';
require_once 'components/pager.inc';
require_once 'components/pane.inc';
require_once 'components/pane_header.inc';

/**
 * Implementation of hook_theme().
 */
function stone_theme() {
  // Consolidate a variety of theme functions under single template types.
  $path = drupal_get_path('theme', 'stone') . '/templates';

  $items['box'] = array(
    'render element' => 'elements',
    'template' => 'box',
    'path' => $path,
  );

  $items['block'] = array(
    'render element' => 'elements',
    'template' => 'box',
    'path' => $path,
  );

  $items['panels_pane'] = array(
    'variables' => array('output' => array(), 'pane' => array(), 'display' => array()),
    'template' => 'box',
    'path' => $path,
  );

  $items['node'] = array(
    'render element' => 'elements',
    'template' => 'object',
    'path' => $path,
  );

  $items['comment'] = array(
    'render element' => 'elements',
    'template' => 'object',
    'path' => $path,
  );

  $items += drupal_find_theme_templates($items, '.tpl.php', $path);

  return $items;
}

/**
 * Overrides theme_panels_default_style_render_region().
 */
function stone_panels_default_style_render_region($vars) {
  return implode('', $vars['panes']);
}

/**
 * Implements hook_element_info_alter().
 */
function stone_element_info_alter(&$type) {
  if (isset($type['text_format'])) {
    $type['text_format']['#process'][] = 'stone_process_format';
  }
}

/**
 * Process callback for the 'text_format' form element. Simplifies the output.
 */
function stone_process_format($element) {
  $element['format']['format']['#title'] = t('Format');
  $element['format']['guidelines']['#access'] = FALSE;

  return $element;
}
