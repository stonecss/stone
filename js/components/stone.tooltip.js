/**
 * Stone: Tooltip
 * A jQuery plugin for tooltips.
 */
;(function ($, window) {
  'use strict';

  var initialized = false;

  /**
   * @method private
   * @name _init
   * @description Initializes plugin
   */
  function _init() {
    if (!initialized) {
      initialized = true;

      $('[data-tooltip]').click(function(e) {
        var selecter = $(this).attr('data-tooltip');

        console.log(selecter);

        $(selecter).toggleClass('Tooltip--show Tooltip--hide');
      });

      $('.Tooltip').each(function(index, element) {
        var tooltip = $(this),
            toggle  = $('<a class="Tooltip-close" href="#"><span class="u-hiddenVisually">Close this tooltip</span>x</a>');

        toggle.prependTo(tooltip);
        toggle.click(function(e) {
          tooltip.toggleClass('Tooltip--show Tooltip--hide');
          e.preventDefault();
          return false;
        });
      });
    }
  }

  $.Tooltip = function() {
    return _init.apply(this);
  };

})(jQuery, window);
