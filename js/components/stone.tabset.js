/**
 * Stone: Tabset
 * A jQuery plugin for tabsets.
 */
;(function ($, window) {
  'use strict';

  var initialized = false;

  /**
   * @method private
   * @name _init
   * @description Initializes plugin
   */
  function _init() {
    if (!initialized) {
      initialized = true;

      $('[data-tabset]').each(function(index, element) {
        var tabset = $(element);
        var tabsetSelecters = tabset.find('[data-tabset-selecter]');
        var tabsetPanels = tabset.find('[data-tabset-panel]');

        if (!tabset.length || !tabsetSelecters.length || !tabsetPanels.length) {
          return false;
        }

        tabsetSelecters.click(function(e) {
          var tabsetSelecter = $(this);
          var target = tabsetSelecter.attr('data-tabset-selecter');

          if (!tabsetSelecter.parent().hasClass('is-active')) {
            tabsetSelecters.parent().removeClass('is-active');
            tabsetSelecter.parent().addClass('is-active');
            tabsetPanels.addClass('u-hidden');
            tabsetPanels.filter('[data-tabset-panel='+target+']').removeClass('u-hidden');
          }

          e.preventDefault();
          return false;
        });
      });
    }
  }

  $.Tabset = function() {
    return _init.apply(this);
  };

})(jQuery, window);
